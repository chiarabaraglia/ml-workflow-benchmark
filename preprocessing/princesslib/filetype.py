import enum


class PrincessDataStorageConversion(enum.Enum):
    PANDAS = 0,
    JSON = 1
