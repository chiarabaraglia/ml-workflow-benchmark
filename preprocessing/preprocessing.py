# Path to the data directory
import os
import shutil
import zipfile
from pathlib import Path
from pickle import dumps

import matplotlib.pyplot as plt
import numpy as np
import redis
import requests
import tensorflow as tf
import yaml
from tensorflow import keras

from princesslib.folder import PrincessDataStorageFolder


def split_data(np_images, np_labels, train_size=0.9, shuffle=True):
    # 1. Get the total size of the dataset
    size = len(np_images)
    # 2. Make an indices array and shuffle it, if required
    indices = np.arange(size)
    if shuffle:
        np.random.shuffle(indices)
    # 3. Get the size of training samples
    train_samples = int(size * train_size)
    # 4. Split data into training and validation sets
    __x_train, __y_train = np_images[indices[:train_samples]], np_labels[indices[:train_samples]]
    __x_valid, __y_valid = np_images[indices[train_samples:]], np_labels[indices[train_samples:]]
    return __x_train, __x_valid, __y_train, __y_valid


def encode_single_sample(img_path, label):
    # 1. Read image
    img = tf.io.read_file(img_path)
    # 2. Decode and convert to grayscale
    img = tf.io.decode_png(img, channels=1)
    # 3. Convert to float32 in [0, 1] range
    img = tf.image.convert_image_dtype(img, tf.float32)
    # 4. Resize to the desired size
    img = tf.image.resize(img, [img_height, img_width])
    # 5. Transpose the image because we want the time
    # dimension to correspond to the width of the image.
    img = tf.transpose(img, perm=[1, 0, 2])
    # 6. Map the characters in label to numbers
    label = char_to_num(tf.strings.unicode_split(label, input_encoding="UTF-8"))
    # 7. Return a dict as our build_model is expecting two inputs
    return {"image": img, "label": label}


def create_train_and_validation_sets():
    __train_dataset = tf.data.Dataset.from_tensor_slices((x_train, y_train))
    __train_dataset = (
        __train_dataset.map(
            encode_single_sample, num_parallel_calls=tf.data.AUTOTUNE
        )
        .batch(batch_size)
        .prefetch(buffer_size=tf.data.AUTOTUNE)
    )

    __validation_dataset = tf.data.Dataset.from_tensor_slices((x_valid, y_valid))
    __validation_dataset = (
        __validation_dataset.map(
            encode_single_sample, num_parallel_calls=tf.data.AUTOTUNE
        )
        .batch(batch_size)
        .prefetch(buffer_size=tf.data.AUTOTUNE)
    )

    return __train_dataset, __validation_dataset


if __name__ == "__main__":
    f = open('../data_environment.yml')
    credentials = yaml.load(f, Loader=yaml.FullLoader)
    client = redis.Redis(host=credentials['redis']['host'], port=int(credentials['redis']['port']))

    url = 'https://github.com/AakashKumarNain/CaptchaCracker/raw/master/captcha_images_v2.zip'

    current_directory = os.getcwd()
    filename = os.path.join(current_directory, "captcha_images_v2.zip")

    r = requests.get(url, allow_redirects=True)
    open(filename, 'wb').write(r.content)

    print('Uncompress to folder ', current_directory)

    zip_file = zipfile.ZipFile(filename)
    zip_file.extractall(current_directory)

    print("Successfully uncompressed")
    data_dir: Path = Path("./captcha_images_v2/")

    # Get list of all the images
    images = sorted(list(map(str, list(data_dir.glob("*.png")))))
    labels = [img.split(os.path.sep)[-1].split(".png")[0] for img in images]
    characters = set(char for label in labels for char in label)
    characters = sorted(list(characters))

    print("Number of images found: ", len(images))
    print("Number of labels found: ", len(labels))
    print("Number of unique characters: ", len(characters))
    print("Characters present: ", characters)

    client.set('images', dumps(images))
    client.set('labels', dumps(labels))
    client.set('characters', dumps(characters))

    # Factor by which the image is going to be downsampled
    # by the convolutional blocks. We will be using two
    # convolution blocks and each block will have
    # a pooling layer which downsample the features by a factor of 2.
    # Hence total downsampling factor would be 4.
    downsample_factor = 4

    batch_size = 16

    # Desired image dimensions
    img_width = 200
    img_height = 50

    # Maximum length of any captcha in the dataset
    max_length = max([len(label) for label in labels])

    client.set('max_length', max_length)

    # Mapping characters to integers
    char_to_num = keras.layers.StringLookup(
        vocabulary=list(characters), mask_token=None
    )

    # Mapping integers back to original characters
    num_to_char = keras.layers.StringLookup(
        vocabulary=char_to_num.get_vocabulary(), mask_token=None, invert=True
    )

    # Splitting data into training and validation sets
    x_train, x_valid, y_train, y_valid = split_data(np.array(images), np.array(labels))

    client.set('x_train', dumps(x_train))
    client.set('y_train', dumps(y_train))
    client.set('x_valid', dumps(x_valid))
    client.set('y_valid', dumps(y_valid))

    # Create and save train and validation dataset
    print("Create and save train and validation dataset")
    train_dataset, validation_dataset = create_train_and_validation_sets()
    train_path = os.path.join(current_directory, "train_dataset")
    validation_path = os.path.join(current_directory, "validation_dataset")
    tf.data.Dataset.save(train_dataset, train_path, compression="GZIP")
    tf.data.Dataset.save(validation_dataset, validation_path, compression="GZIP")

    # Compress train dataset folder
    print("Compress train dataset folder")
    train_filepath = os.path.join(current_directory, "train_dataset.zip")
    shutil.make_archive(train_path, 'zip', train_path)
    shutil.rmtree(train_path, ignore_errors=True)

    # Compress validation dataset folder
    print("Compress validation dataset folder")
    val_filepath = os.path.join(current_directory, "validation_dataset.zip")
    shutil.make_archive(validation_path, 'zip', validation_path)
    shutil.rmtree(validation_path, ignore_errors=True)

    data_storage_url = credentials['datastorage']['base_url']
    folder = PrincessDataStorageFolder(base_url=data_storage_url).set_folder(credentials['datastorage']['data_folder'])

    # Store compressed training set on data storage endpoint
    print("Store compressed training set on data storage endpoint")
    its_ok_t, train_upload = folder.upload(train_filepath)
    if not its_ok_t:
        raise Exception("An error occurred uploading training set: " + train_upload)

    # Store compressed validation set on data storage endpoint
    print("Store compressed validation set on data storage endpoint")
    its_ok_v, val_upload = folder.upload(val_filepath)
    if not its_ok_v:
        raise Exception("An error occurred uploading validation set: " + val_upload)

    _, ax = plt.subplots(4, 4, figsize=(10, 5))
    for batch in train_dataset.take(1):
        images = batch["image"]
        labels = batch["label"]
        for i in range(16):
            img = (images[i] * 255).numpy().astype("uint8")
            label = tf.strings.reduce_join(num_to_char(labels[i])).numpy().decode("utf-8")
            ax[i // 4, i % 4].imshow(img[:, :, 0].T, cmap="gray")
            ax[i // 4, i % 4].set_title(label)
            ax[i // 4, i % 4].axis("off")
    plt.show()

    shutil.rmtree(os.path.join(current_directory, "captcha_images_v2"))
    os.remove(os.path.join(current_directory, "captcha_images_v2.zip"))
    os.remove(train_filepath)
    os.remove(val_filepath)
    f.close()
    print("Preprocessing successfully completed!")
