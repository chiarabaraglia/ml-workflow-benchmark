matplotlib==3.6.2
numpy==1.23.5
PyYAML==6.0
pandas==1.5.2

redis==4.3.4

tensorflow==2.10.1
keras==2.10.0