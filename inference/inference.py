import json
import os
import shutil
from pickle import loads

import numpy as np
import redis
import tensorflow as tf
import yaml
from matplotlib import pyplot as plt
from tensorflow import keras

from princesslib.folder import PrincessDataStorageFolder


class CTCLayer(tf.keras.layers.Layer):
    def __init__(self, name=None, **kwargs):
        super().__init__(name=name, **kwargs)
        self.loss_fn = tf.keras.backend.ctc_batch_cost

    def call(self, y_true, y_pred):
        # Compute the training-time loss value and add it
        # to the layer using `self.add_loss()`.
        batch_len = tf.cast(tf.shape(y_true)[0], dtype="int64")
        input_length = tf.cast(tf.shape(y_pred)[1], dtype="int64")
        label_length = tf.cast(tf.shape(y_true)[1], dtype="int64")

        input_length = input_length * tf.ones(shape=(batch_len, 1), dtype="int64")
        label_length = label_length * tf.ones(shape=(batch_len, 1), dtype="int64")

        loss = self.loss_fn(y_true, y_pred, input_length, label_length)
        self.add_loss(loss)

        # At test time, just return the computed predictions
        return y_pred


# A utility function to decode the output of the network
def decode_batch_predictions(pred):
    input_len = np.ones(pred.shape[0]) * pred.shape[1]
    # Use greedy search. For complex tasks, you can use beam search
    results = keras.backend.ctc_decode(pred, input_length=input_len, greedy=True)[0][0][
              :, :max_length
              ]
    # Iterate over the results and get back the text
    output_text = []
    for res in results:
        res = tf.strings.reduce_join(num_to_char(res)).numpy().decode("utf-8")
        output_text.append(res)
    return output_text


if __name__ == "__main__":
    f = open('../data_environment.yml')
    credentials = yaml.load(f, Loader=yaml.FullLoader)
    client = redis.Redis(host=credentials['redis']['host'], port=int(credentials['redis']['port']))
    parent_directory = os.path.dirname(os.getcwd())
    data_storage_url = credentials['datastorage']['base_url']
    folder = PrincessDataStorageFolder(base_url=data_storage_url).set_folder(credentials['datastorage']['data_folder'])

    json_model = json.loads(client.get('model_architecture'))
    weights_list: list = loads(client.get("weights_list"))

    model = tf.keras.models.model_from_json(json_model, custom_objects={"CTCLayer": CTCLayer})
    model.set_weights(weights_list)

    model.compile()

    validation_path = os.path.join(parent_directory, "downloaded_validation_dataset")

    print("Download compressed validation set")
    folder.download("validation_dataset.zip", parent_directory)

    print("Unpack validation set")
    shutil.unpack_archive(os.path.join(parent_directory, "validation_dataset.zip"), extract_dir=validation_path)

    print("Load validation set")
    validation_dataset = tf.data.Dataset.load(validation_path, compression="GZIP")

    characters = loads(client.get('characters'))
    max_length = int(client.get('max_length'))

    # Mapping characters to integers
    char_to_num = keras.layers.StringLookup(
        vocabulary=list(characters), mask_token=None
    )

    # Mapping integers back to original characters
    num_to_char = keras.layers.StringLookup(
        vocabulary=char_to_num.get_vocabulary(), mask_token=None, invert=True
    )

    # Get the prediction model by extracting layers till the output layer
    prediction_model = keras.models.Model(
        model.get_layer(name="image").input, model.get_layer(name="dense2").output
    )
    prediction_model.summary()

    #  Let's check results on some validation samples
    for batch in validation_dataset.take(1):
        batch_images = batch["image"]
        batch_labels = batch["label"]

        preds = prediction_model.predict(batch_images)
        pred_texts = decode_batch_predictions(preds)

        orig_texts = []
        for label in batch_labels:
            label = tf.strings.reduce_join(num_to_char(label)).numpy().decode("utf-8")
            orig_texts.append(label)

        _, ax = plt.subplots(4, 4, figsize=(15, 5))
        for i in range(len(pred_texts)):
            img = (batch_images[i, :, :, 0] * 255).numpy().astype(np.uint8)
            img = img.T
            title = f"Prediction: {pred_texts[i]}"
            ax[i // 4, i % 4].imshow(img, cmap="gray")
            ax[i // 4, i % 4].set_title(title)
            ax[i // 4, i % 4].axis("off")
    plt.show()

    shutil.rmtree(validation_path)
    os.remove(os.path.join(parent_directory, "validation_dataset.zip"))
    f.close()

    print("Inference successfully completed!")
