import json
import time

import pandas as pd
import requests
from princesslib.filetype import PrincessDataStorageConversion


class PrincessDataStorageFolder:
    """
    This class manages the interaction with the Princess Data Storage folder
    """

    def __init__(self, base_url):
        self.__base_url = base_url
        self.__current_folder = None

    def set_folder(self, folder: str):
        self.__current_folder = folder
        return self

    def upload(self, file_path: str) -> (bool, str):
        request_url = self.__base_url + "/_folder" + self.__current_folder + "/_upload"
        retry = 1
        success = False

        while not success and retry < 5:
            try:
                with open(file_path, "rb") as file_data:
                    response = requests.post(url=request_url, files={'file': file_data})
                    success = True

                    if response.ok:
                        return True, "Ok"
                    else:
                        return False, str(response.status_code) + ": " + str(response.reason)

            except Exception as e:
                wait = retry * 10;
                time.sleep(wait)
                retry += 1

        return False, "timeout reached"

    def download(self, filename: str, path: str, convert: PrincessDataStorageConversion = None, **kwargs):
        request_url = self.__base_url + "/_folder" + self.__current_folder + "/_download"
        retry = 1
        success = False

        while not success and retry < 5:
            try:
                response = requests.get(url=request_url, params={'q': 'filename=={}'.format(filename)})
                success = True

                with open(path + "/" + filename, "wb") as file_data:
                    file_data.write(response.content)

                if convert == PrincessDataStorageConversion.PANDAS:
                    return pd.read_csv(path + "/" + filename, sep=kwargs['sep'], header=kwargs['header'],
                                       quotechar=kwargs['quotechar'] if 'quotechar' in kwargs else '"')
                elif convert == PrincessDataStorageConversion.JSON:
                    with open(path + "/" + filename, "r") as file_data:
                        return json.load(file_data)
                else:
                    return True

            except Exception as e:
                wait = retry * 10;
                time.sleep(wait)
                retry += 1

        return False
