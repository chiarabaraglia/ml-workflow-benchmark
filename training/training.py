import json
import os
import shutil
from pickle import dumps

import redis
import tensorflow as tf
import yaml
from tensorflow import keras

from princesslib.folder import PrincessDataStorageFolder


class CTCLayer(tf.keras.layers.Layer):
    def __init__(self, name=None, **kwargs):
        super().__init__(name=name, **kwargs)
        self.loss_fn = tf.keras.backend.ctc_batch_cost

    def call(self, y_true, y_pred):
        # Compute the training-time loss value and add it
        # to the layer using `self.add_loss()`.
        batch_len = tf.cast(tf.shape(y_true)[0], dtype="int64")
        input_length = tf.cast(tf.shape(y_pred)[1], dtype="int64")
        label_length = tf.cast(tf.shape(y_true)[1], dtype="int64")

        input_length = input_length * tf.ones(shape=(batch_len, 1), dtype="int64")
        label_length = label_length * tf.ones(shape=(batch_len, 1), dtype="int64")

        loss = self.loss_fn(y_true, y_pred, input_length, label_length)
        self.add_loss(loss)

        # At test time, just return the computed predictions
        return y_pred


def create_generator(list_of_arrays):
    for i in list_of_arrays:
        yield i


if __name__ == "__main__":
    f = open('../data_environment.yml')
    credentials = yaml.load(f, Loader=yaml.FullLoader)
    client = redis.Redis(host=credentials['redis']['host'], port=int(credentials['redis']['port']))
    parent_directory = os.path.dirname(os.getcwd())

    print("Retrieve model architecture from database")
    json_model = json.loads(client.get('model_architecture'))

    print("Load model architecture")
    model = tf.keras.models.model_from_json(json_model, custom_objects={"CTCLayer": CTCLayer})
    model.compile()

    batch_size = 16

    # Desired image dimensions
    img_width = 200
    img_height = 50

    epochs = 100
    early_stopping_patience = 10
    # Add early stopping
    early_stopping = keras.callbacks.EarlyStopping(
        monitor="val_loss", patience=early_stopping_patience, restore_best_weights=True
    )

    data_storage_url = credentials['datastorage']['base_url']
    folder = PrincessDataStorageFolder(base_url=data_storage_url).set_folder(credentials['datastorage']['data_folder'])

    train_path = os.path.join(parent_directory, "downloaded_train_dataset")
    validation_path = os.path.join(parent_directory, "downloaded_validation_dataset")

    os.mkdir(train_path, mode=0o777)
    os.mkdir(validation_path, mode=0o777)

    print("Download compressed training set")
    folder.download("train_dataset.zip", parent_directory)
    print("Download compressed validation set")
    folder.download("validation_dataset.zip", parent_directory)

    print("Unpack training set")
    shutil.unpack_archive(os.path.join(parent_directory, "train_dataset.zip"), extract_dir=train_path)
    print("Unpack validation set")
    shutil.unpack_archive(os.path.join(parent_directory, "validation_dataset.zip"), extract_dir=validation_path)

    print("Load training set")
    train_dataset = tf.data.Dataset.load(train_path, compression="GZIP")
    print("Load validation set")
    validation_dataset = tf.data.Dataset.load(validation_path, compression="GZIP")

    # Train the model
    history = model.fit(
        train_dataset,
        validation_data=validation_dataset,
        epochs=epochs,
        callbacks=[early_stopping],
    )

    weights_list = model.get_weights()
    print(type(weights_list))
    serialized = dumps(weights_list)
    print(type(serialized))
    client.set("weights_list", serialized)

    shutil.rmtree(train_path)
    shutil.rmtree(validation_path)
    os.remove(os.path.join(parent_directory, "train_dataset.zip"))
    os.remove(os.path.join(parent_directory, "validation_dataset.zip"))
    f.close()

    print("Training successfully completed.")
